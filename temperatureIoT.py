from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import json
import RPi.GPIO as GPIO
import uuid
import platform
import Adafruit_DHT

host = "[]"
rootCAPath = "[]"
certificatePath = "[]"
privateKeyPath = "[]"
clientId = platform.node()
topic = "Test/DHT22"

DHT22Pin = 11
BlueLedPin = 23
RedLedPin = 24
GreenLedPin = 25

GPIO.setmode(GPIO.BCM)
GPIO.setup(RedLedPin, GPIO.OUT)
GPIO.setup(GreenLedPin, GPIO.OUT)
GPIO.setup(BlueLedPin, GPIO.OUT)

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.ERROR)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None

myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureEndpoint(host, 8883)
myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()
time.sleep(3)

guid = uuid.uuid4()

try:
    while True:
        humidity, temperature = Adafruit_DHT.read_retry(22, DHT22Pin)
        if humidity is not None and temperature is not None:
            temperature = temperature * 9/5.0 + 32
            print('*************************************************************************')
            print('Temp={0:.3g}*  Humidity={1:.3g}%'.format(temperature, humidity))
            print('                      *********************                              ')
            python_object = {
                'DHT22Status': 'New Reading...',
                'DeviceName' : platform.node(),
                'SessionId' : str(guid),
                'Time' : time.strftime("%m/%d/%Y %H:%M"),
                'Temperature' : '{0:.5g}'.format(temperature),
		        'Humidity' : '{0:.5g}'.format(humidity)
                }
            json_string = json.dumps(python_object)
            myAWSIoTMQTTClient.publish(topic,json_string,1)
            print('Published topic %s: %s\n' % (topic, json_string))
            print('*************************************************************************')

            if temperature <= 68:
                GPIO.output(BlueLedPin, True)
                GPIO.output(GreenLedPin, False)
                GPIO.output(RedLedPin, False)
            elif temperature > 68 and temperature <= 78:
                GPIO.output(BlueLedPin, False)
                GPIO.output(GreenLedPin, True)
                GPIO.output(RedLedPin, False)
            else:
                GPIO.output(BlueLedPin, False)
                GPIO.output(GreenLedPin, False)
                GPIO.output(RedLedPin, True)
                    
        else:
            print('Failed to get a reading. Try again!')

        time.sleep(900)
except:
        GPIO.cleanup()
        myAWSIoTMQTTClient.disconnect()
