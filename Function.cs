using System;
using System.Linq;
using System.Collections.Generic;
using Amazon.Lambda.Core;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using System.Net;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace IoTDynamoDBToAPIGateway
{
    public class Function
    {
        private readonly AmazonDynamoDBClient client;
        private static ILambdaLogger logger;
        private readonly string tableName;
        private readonly string partitionkeyValue;
        private readonly string partitionkeyName;

        public Function()
        {
            client = new AmazonDynamoDBClient();
            tableName = Environment.GetEnvironmentVariable("TableName");
            partitionkeyName = Environment.GetEnvironmentVariable("PartitionKeyName");
            partitionkeyValue = Environment.GetEnvironmentVariable("PartitionkeyValue");
        }

        public string FunctionHandler(ILambdaContext context)
        {
            try
            {
                logger = context.Logger;
                logger.LogLine("*** Start executing GetLatestRecord ***");

                Table ioTDemo = Table.LoadTable(client, tableName);

                Search searchResultExecutor = RetrieveLatestReading(ioTDemo);

                List<Document> resultSet = searchResultExecutor.GetNextSetAsync().Result;
                logger.LogLine("*** Finish executing GetLatestRecord ***");

                var resultJSON = ProcessResultDocument(resultSet);
                logger.LogLine("Result : " + resultJSON);

                return resultJSON;
            }
            catch (Exception e)
            {
                logger.LogLine(e.Message);

                return "Failed to retrieve IoT DHT22 latest reading.";
            }
        }

        private Search RetrieveLatestReading(Table ioTDemo)
        {
            logger.LogLine("*** start executing RetrieveLatestReading ***");

            Expression keyExpression = new Expression()
            {
                ExpressionAttributeNames = new Dictionary<string, string>()
                {
                    ["#DeviceName"] = partitionkeyName,
                },
                ExpressionAttributeValues = new Dictionary<string, DynamoDBEntry>()
                {
                    [":DeviceName"] = partitionkeyValue,
                },
                ExpressionStatement = "#DeviceName = :DeviceName"
            };

            QueryOperationConfig config = new QueryOperationConfig()
            {
                AttributesToGet = new List<string> { "Payload" },
                KeyExpression = keyExpression,
                Select = SelectValues.SpecificAttributes,
                Limit = 1,
                BackwardSearch = true
            };

            Search search = ioTDemo.Query(config);

            logger.LogLine("start executing RetrieveLatestReading...");

            return search;
        }

        private static string ProcessResultDocument(List<Document> results)
        {
            logger.LogLine("ProcessResultDocument : result count: " + results.Count.ToString());

            var result = results.LastOrDefault();

            if (result == null)
            {
                return string.Empty;
            }
            else
            {
                return result.ToJson();
            }
        }
    }

    public class LambdaResponse
    {
        public HttpStatusCode statusCode { get; set; }
        public Dictionary<string, string> headers { get; set; }
        public string body { get; set; }
    }
}