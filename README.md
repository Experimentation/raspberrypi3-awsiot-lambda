# RaspberryPi3-AWSIoT-Lambda

> This repository contains the backend of my demo S3 website www.justfortest.com

I'm using Python GPIO and Adafruit_DHT libraries to measure the room temperature & humidity via a DHT22 sensor.

# The program also controls three LEDs
- Red is lit if the room temperature is above 78.
- Green is lit if the room temperature is between 68 and 78.
- Blue is lit if the room temperature is below or equal to 68.

It sends the temperature & humidity readings from the Raspberry Pi 3 to AWS IoT Service using AWS MQTT Python library.

Using an AWS IoT Act it persist the reading data into a DynamoDB table.

Once the API Gateway receive a request, it communicates with Lambda function (which is written in C#) to pull the last reading from DynamoDB table.

Finally, API Gateway send back the last temperature & humidity reading in JSON format.
